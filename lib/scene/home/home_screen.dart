import 'package:flutter/material.dart';

import 'package:font_awesome_flutter/font_awesome_flutter.dart';

import '../../config/model/data_model.dart';
import '../../config/prefrences/prefrences_handler.dart';
import '../../theme.dart';
import 'product_handler.dart';

class HomePage extends StatefulWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  final _pref = PrefrencesHanlder();
  final _product = ProductHandler();
  late Future<List<DataModel>> futureProduct;
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    futureProduct = _product.getAllProduct();
  }

  Future<void> showLogoutDialog(String title, String message) async {
    return showDialog<void>(
      context: context,
      barrierDismissible: false, // user must tap button!
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text(title),
          content: SingleChildScrollView(
            child: Text(message),
          ),
          actions: [
            TextButton(
              child: const Text('Logout'),
              onPressed: () {
                _pref.logout();
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Home'),
        actions: [
          IconButton(
              onPressed: () {
                showLogoutDialog(
                    "Logout alert", "Anda yakin ingin keluar dari aplikasi?");
              },
              icon: const Icon(Icons.logout_rounded))
        ],
      ),
      body: SafeArea(
        child: FutureBuilder<List<DataModel>>(
          future: _product.getAllProduct(),
          builder: (context, snapshot) {
            if (snapshot.hasData) {
              return ListView.builder(
                  itemCount: snapshot.data!.length,
                  itemBuilder: (BuildContext context, int index) {
                    return ListTile(
                      leading: Image.network(snapshot.data![index].thumbnail,
                          width: 100, height: 50, fit: BoxFit.fill),
                      title: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            snapshot.data![index].title,
                            style: heading6,
                          ),
                          Text(
                            "\Price \$${snapshot.data![index].price}",
                            style: heading6,
                          ),
                          Text(
                            "\Discount ${snapshot.data![index].discountPercentage} \%",
                            style: heading6,
                          ),
                          Text(
                            snapshot.data![index].description,
                            style: regular16pt,
                          )
                        ],
                      ),
                    );
                  });
            } else if (snapshot.hasError) {
              return Text("data error");
            }

            return const CircularProgressIndicator();
          },
        ),
      ),
    );
  }
}
