import 'dart:convert';

import '../../config/api/api_provider.dart';
import '../../config/model/data_model.dart';

class ProductHandler {
  Future<List<DataModel>> getAllProduct() async {
    final response = await ApiProvider().getMethod('products');
    List<dynamic> list = response['products'];

    late List<DataModel> listData = <DataModel>[];
    for (var i = 0; i < list.length; i++) {
      int id = list[i]['id'];
      String title = list[i]['title'];
      String description = list[i]['description'];
      int price = list[i]['price'];
      double discountPercentage = list[i]['discountPercentage'];
      String image = list[i]['thumbnail'];
      listData.add(DataModel(
          id: id,
          title: title,
          description: description,
          price: price,
          discountPercentage: discountPercentage,
          thumbnail: image));
    }

    print(">> $listData");

    return listData;
  }
}
