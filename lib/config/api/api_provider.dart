import 'dart:convert';

import 'package:http/http.dart';
import 'package:pretty_http_logger/pretty_http_logger.dart';

import '../prefrences/constant.dart';
import '../prefrences/getter_value.dart';
import '../route/route_link.dart';

class ApiProvider {
  static final HttpWithMiddleware _httpClient =
      HttpWithMiddleware.build(middlewares: [
    HttpLogger(logLevel: LogLevel.BODY),
  ]);

  Future postMethod(String path, String endpoint, String stream) async {
    final response =
        await _httpClient.post(Uri.parse('$BASEURL$path/$endpoint'),
            headers: <String, String>{
              'Content-Type': "application/json",
            },
            body: stream);

    //
    Map<String, dynamic> myBody = jsonDecode(response.body);
    if (response.statusCode == 200) {
      return myBody;
    } else {
      return myBody;
    }
  }

  Future getMethod(String enpoint) async {
    final response = await _httpClient.get(Uri.parse('$BASEURL$enpoint'));

    Map<String, dynamic> myBody = jsonDecode(response.body);
    if (response.statusCode == 200) {
      return myBody;
    } else {
      return myBody;
    }
  }
}
