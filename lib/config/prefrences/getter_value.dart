import 'package:shared_preferences/shared_preferences.dart';

import 'constant.dart';

class GetterValue {
  static SharedPreferences? _prefs;

  static Future<SharedPreferences?> init() async {
    _prefs = await SharedPreferences.getInstance();
    return _prefs;
  }

  // ambil data dari prefrences
  static String? getFirstName(String firstname) => _prefs?.getString(TAG_FIRSTNAMA);
  static String? getLastName(String lastname) => _prefs?.getString(TAG_LASTNAME);
  static String? getToken(String token) => _prefs?.getString(TAG_TOKEN);
}