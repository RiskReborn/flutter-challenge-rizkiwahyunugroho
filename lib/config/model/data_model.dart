class DataModel {
  final int id;
  final String title;
  final String description;
  final int price;
  final String thumbnail;
  final double discountPercentage;

  const DataModel(
      {required this.id,
      required this.title,
      required this.description,
      required this.price,
      required this.thumbnail,
      required this.discountPercentage});

  factory DataModel.fromJson(Map<String, dynamic> json) {
    return DataModel(
      id: json['id'],
      title: json['title'],
      description: json['description'],
      price: json['price'],
      discountPercentage: json['discountPercentage'],
      thumbnail: json['thumbnail'],
    );
  }
}
