import '../../scene/auth/login_screen.dart';
import '../../scene/home/home_screen.dart';
import '../../scene/splash/splash_screen.dart';

final routes = {
  '/': (context) => const SplashScreen(),
  '/login': (context) => const LoginPage(),
  '/home': (context) => const HomePage(),
};
